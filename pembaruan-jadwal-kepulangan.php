<?php
/*
 * (c) 2014 Sisdik, <info@sisdik.com>
 */

require_once 'konfigurasi.php';

$berkasKepulangan = JADWAL_DIR . BERKAS_KEPULANGAN;
$jadwalKepulangan = file_get_contents('https://' . FTP_SERVER . '/_api/jadwal-kepulangan/' . API_TOKEN);

$jadwalHandle = fopen($berkasKepulangan, 'w');
$logHandle = fopen(LOG_MANUAL, 'a+');

fwrite($jadwalHandle, $jadwalKepulangan);
fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Memperbarui jadwal kepulangan\n");

fclose($jadwalHandle);
fclose($logHandle);

$returnValue['pesan'][] = "Informasi jadwal dan mesin kehadiran di mesin wakil-sisdik berhasil diperbarui.";

print json_encode($returnValue);
