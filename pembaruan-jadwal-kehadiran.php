<?php
/*
 * (c) 2014 Sisdik, <info@sisdik.com>
 */

require_once 'konfigurasi.php';

$berkasKehadiran = JADWAL_DIR . BERKAS_KEHADIRAN;
$jadwalKehadiran = file_get_contents('https://' . FTP_SERVER . '/_api/jadwal-kehadiran/' . API_TOKEN);

$jadwalHandle = fopen($berkasKehadiran, 'w');
$logHandle = fopen(LOG_MANUAL, 'a+');

fwrite($jadwalHandle, $jadwalKehadiran);
fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Memperbarui jadwal kehadiran\n");

fclose($jadwalHandle);
fclose($logHandle);

$returnValue['pesan'][] = "Informasi jadwal dan mesin kehadiran di mesin wakil-sisdik berhasil diperbarui.";

print json_encode($returnValue);
