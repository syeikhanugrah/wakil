<?php
/*
 * (c) 2017 Sisdik, <ihsan@sisdik.com>
 */

require_once 'konfigurasi.php';

require_once 'lib/TADFactory.php';
require_once 'lib/TAD.php';
require_once 'lib/TADResponse.php';
require_once 'lib/Providers/TADSoap.php';
require_once 'lib/Providers/TADZKLib.php';
require_once 'lib/Exceptions/ConnectionError.php';
require_once 'lib/Exceptions/FilterArgumentError.php';
require_once 'lib/Exceptions/UnrecognizedArgument.php';
require_once 'lib/Exceptions/UnrecognizedCommand.php';

$pesan = array();
$cliOptions = getopt('m:c:t::a::k::l::f::');
// m - IP mesin kehadiran
// c - communication key mesin kehadiran
// t - ID tahun masuk
// a - penanda akademik (selain dari '0')
// k - ID tingkat
// l - ID kelas
// f - filter siswa (ID Sisdik)

$mesinKehadiran = $cliOptions['m'];
$commKey = $cliOptions['c'];

$tahunMasuk = array_key_exists('t', $cliOptions) ? $cliOptions['t'] : '0';

$akademik = array_key_exists('a', $cliOptions) ? $cliOptions['a'] : '0';
$tingkat = array_key_exists('k', $cliOptions) ? $cliOptions['k'] : '0';
$kelas = array_key_exists('l', $cliOptions) ? $cliOptions['l'] : '0';

$filter = array_key_exists('f', $cliOptions) ? $cliOptions['f'] : '0';

$apiUrlSiswa = 'https://' . FTP_SERVER . '/_api/user-mesin-kehadiran/' . API_TOKEN . '/';
$apiUrlPTK = 'https://' . FTP_SERVER . '/_api/user-mesin-kehadiran-ptk/' . API_TOKEN . '/';

if ($tahunMasuk != '0' && $akademik == '0') {
    $userKehadiranJson = file_get_contents($apiUrlSiswa . "?tahun=$tahunMasuk&filter=$filter");
} elseif ($tahunMasuk == '0' && $akademik != '0') {
    $userKehadiranJson = file_get_contents($apiUrlSiswa . "?akademik=1&tingkat=$tingkat&kelas=$kelas&filter=$filter");
} else {
    $userKehadiranJson = file_get_contents($apiUrlPTK);
}

$logHandle = fopen(LOG_USER_KEHADIRAN, 'a+');

if ($mesinKehadiran != '') {
    fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Menyimpan user kehadiran ke mesin kehadiran {$mesinKehadiran} ...\n");

    $options = array(
        'ip' => $mesinKehadiran,
        'com_key' => $commKey,
    );

    $tadFactory = new TADPHP\TADFactory($options);
    $tad = $tadFactory->get_instance();

    if ($tad !== null) {
        try {
            $userKehadiran = json_decode($userKehadiranJson, true);

            if (count($userKehadiran) <= 0) {
                $pesan[] = "Tidak ada data user yang bisa disimpan.";
            } else {
                foreach ($userKehadiran as $user) {
                    fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Menyimpan user kehadiran {$user['id']}:{$user['nama']}\n");

                    $tad->set_user_info(array(
                        'pin' => $user['id'],
                        'name' => $user['nama'],
                    ));
                }

                $pesan[] = "Berhasil memerintahkan wakil sisdik untuk menyimpan user kehadiran ke mesin {$mesinKehadiran}.";
            }
        } catch (TADPHP\Exceptions\ConnectionError $exception) {
            fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Mesin kehadiran {$mesinKehadiran} tidak dapat dijangkau\n");
            $pesan[] = "Mesin kehadiran {$mesinKehadiran} tidak dapat dijangkau.";
        }
    } else {
        fwrite($logHandle, '[' . date('Y-m-d H:i:s') . "]: Tak bisa terhubung ke mesin kehadiran {$mesinKehadiran} untuk menyimpan user\n");
        $pesan[] = "Tak bisa terhubung ke mesin kehadiran {$mesinKehadiran} untuk menyimpan user kehadiran. Periksa koneksi jaringan lokal dan internet.";
    }
}

fclose($logHandle);

print json_encode($pesan);
